# RPG characters

This project presents an initial design of a role player game that would help developers who wishes to use this design as a back-end of their game.
This design allow developers to add more character classes and equipments such as armor and weapons.


## Functionality
The application contains three different character classes and three types of weapons and armor. each character has a total of 4 slots which can be used as below:

* Weapon slot
* Head armor
* Body armor
* Legs armor

The equipment provide the main character with bonuses that incsrease the character stats and damage output.



# Testing
The application provides a menu with 3 different characters to check the stats values and how they change as the character equip change weapon and armor. 
Below are samples of how the output looks during run time: 

- Main menu

![](demo/mainMenu.png)
- Character list

![](demo/characterList.png)
- Weapon details

![](demo/weapon.png)
- Armor details

![](demo/armor.png)
- character details

![](demo/characterDetails.png)
