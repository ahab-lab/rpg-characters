package dk.experis.Java.equipments.armor;

public class ClothArmor extends Armor {

    public ClothArmor(String name, int level) {
        super(name, level);
        setStats(new BonusStats(10, 3, 1, 0, 5, 2, 1, 0, level));
        setType("Cloth");
    }
}
