package dk.experis.Java.equipments.armor;

import dk.experis.Java.equipments.Item;

public class Armor extends Item {

    private BonusStats stats;

    public Armor(String name, int level) {
        super(name, level);
    }

    public BonusStats getStats() {
        return stats;
    }

    public void setStats(BonusStats stats) {
        this.stats = stats;
    }

}