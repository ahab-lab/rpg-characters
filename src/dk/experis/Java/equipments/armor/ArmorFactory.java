package dk.experis.Java.equipments.armor;

public class ArmorFactory {

    public Armor makeArmor(String name, String type) {
        return switch (type) {
            case "leather" -> new LeatherArmor(name, 30);
            case "cloth" -> new ClothArmor(name, 20);
            case "plate" -> new PlateArmor(name, 5);
            default -> null;
        };
    }
}