package dk.experis.Java.equipments.armor;

public class PlateArmor extends Armor {

    public PlateArmor(String name, int level) {
        super(name, level);
        setStats(new BonusStats(30, 0, 1, 3, 12, 0, 1, 2, level));
        setType("Plate");
    }
}
