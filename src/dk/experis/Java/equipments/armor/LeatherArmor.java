package dk.experis.Java.equipments.armor;

public class LeatherArmor extends Armor {

    public LeatherArmor(String name, int level) {
        super(name, level);
        setStats(new BonusStats(20, 0, 3, 1, 8, 0, 2, 1, level));
        setType("Leather");
    }
}
