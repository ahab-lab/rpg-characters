package dk.experis.Java.equipments.armor;

public enum SlotType {
    INVENTORY, BODY, LEGS, HEAD
}
