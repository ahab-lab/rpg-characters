package dk.experis.Java.equipments.armor;

public class BonusStats {
    private final int hpPerLvl;
    private final int intPerLvl;
    private final int dexPerLvl;
    private final int strPerLvl;
    private final int lvl;
    private final int hpBonus;
    private final int intBonus;
    private final int dexBonus;
    private final int strBonus;
    private SlotType slot;

    public BonusStats(int hpBonus, int intBonus, int dexBonus, int strBonus, int hpPerLvl, int intPerLvl, int dexPerLvl, int strPerLvl, int lvl) {
        this.hpBonus = hpBonus;
        this.intBonus = intBonus;
        this.dexBonus = dexBonus;
        this.strBonus = strBonus;
        this.hpPerLvl = hpPerLvl;
        this.intPerLvl = intPerLvl;
        this.dexPerLvl = dexPerLvl;
        this.strPerLvl = strPerLvl;
        this.lvl = lvl;
        slot = SlotType.INVENTORY;
    }

    public int getStrBonus() {
        return strBonus;
    }

    public int getDexBonus() {
        return dexBonus;
    }

    public int getIntBonus() {
        return intBonus;
    }

    public int getHpBonus() {
        return hpBonus;
    }

    public int getFinalHp(SlotType slot) {
        return calculateFinalValue(slot, hpBonus, hpPerLvl);
    }

    public int getFinalInt(SlotType slot) {
        return calculateFinalValue(slot, intBonus, intPerLvl);
    }

    public int getFinalDex(SlotType slot) {
        return calculateFinalValue(slot, dexBonus, dexPerLvl);
    }

    public int getFinalStr(SlotType slot) {
        return calculateFinalValue(slot, strBonus, strPerLvl);
    }

    private int calculateFinalValue(SlotType slot, int strBonus, int strPerLvl) {
        return (int) switch (slot) {
            case BODY, INVENTORY -> strBonus + (strPerLvl * lvl);
            case HEAD -> (strBonus + (strPerLvl * lvl)) * 0.8;
            case LEGS -> (strBonus + (strPerLvl * lvl)) * 0.6;
            default -> 0;

        };
    }

    public SlotType getSlot() {
        return slot;
    }

    public void setSlot(SlotType slot) {
        this.slot = slot;
    }
}