package dk.experis.Java.equipments.weapons;

public class RangeWeapon extends Weapon {

    public RangeWeapon(String name, int level) {
        super(name, level);
        setAddedPercentage(3);
        setBaseDamage(5);
        setType("Range");
    }
}
