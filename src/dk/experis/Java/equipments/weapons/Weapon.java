package dk.experis.Java.equipments.weapons;

import dk.experis.Java.equipments.Item;

public class Weapon extends Item {
    private int baseDamage;
    private int scaleDamage;
    private int addedPercentage;

    public Weapon(String name, int level) {
        super(name, level);
        scaleDamage();
    }

    public void scaleDamage() {
        scaleDamage = baseDamage + (getLevel() * addedPercentage);
    }

    public int getDamage() {
        scaleDamage();
        return scaleDamage;
    }

    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }

    public void setAddedPercentage(int addedPercentage) {
        this.addedPercentage = addedPercentage;
    }
}