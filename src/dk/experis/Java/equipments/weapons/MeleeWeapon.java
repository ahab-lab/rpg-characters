package dk.experis.Java.equipments.weapons;

public class MeleeWeapon extends Weapon {

    public MeleeWeapon(String name, int level) {
        super(name, level);
        setAddedPercentage(2);
        setBaseDamage(15);
        setType("Melee");
    }
}
