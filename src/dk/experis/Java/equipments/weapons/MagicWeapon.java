package dk.experis.Java.equipments.weapons;

public class MagicWeapon extends Weapon {

    public MagicWeapon(String name, int level) {
        super(name, level);
        setAddedPercentage(2);
        setBaseDamage(25);
        setType("Magic");
    }
}