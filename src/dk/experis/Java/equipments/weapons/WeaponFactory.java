package dk.experis.Java.equipments.weapons;

public class WeaponFactory {

    public Weapon makeWeapon(String name, String weapon) {
        return switch (weapon) {
            case "range" -> new RangeWeapon(name, 5);
            case "melee" -> new MeleeWeapon(name, 5);
            case "magic" -> new MagicWeapon(name, 14);
            default -> null;
        };
    }
}