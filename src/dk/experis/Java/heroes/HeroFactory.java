package dk.experis.Java.heroes;

public class HeroFactory {

    public Hero makeHero(String hero) {

        return switch (hero) {
            case "ranger" -> new Ranger();
            case "warrior" -> new Warrior();
            case "mage" -> new Mage();
            default -> null;
        };
    }
}