package dk.experis.Java.heroes;

import dk.experis.Java.heroes.properties.Stats;

public class Mage extends Hero {

    public Mage() {
        setStats(new Stats(100, 2, 3, 10, 1, 0,
                15, 1, 2, 5));
        setName("Mage");
    }
}