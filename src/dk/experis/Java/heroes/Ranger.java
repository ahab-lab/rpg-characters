package dk.experis.Java.heroes;

import dk.experis.Java.heroes.properties.Stats;

public class Ranger extends Hero {

    public Ranger() {
        setStats(new Stats(120, 5, 10, 2, 1, 0,
                20, 2, 5, 1));
        setName("Ranger");
    }
}