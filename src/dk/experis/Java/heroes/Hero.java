package dk.experis.Java.heroes;

import dk.experis.Java.equipments.Item;
import dk.experis.Java.equipments.armor.SlotType;
import dk.experis.Java.heroes.properties.Slots;
import dk.experis.Java.heroes.properties.Stats;
import dk.experis.Java.heroes.properties.TotalModifiedStats;

import java.util.ArrayList;

public abstract class Hero implements  TotalModifiedStats {

    protected ArrayList<Item> inventory;

    private Stats stats;
    private Slots slots;
    private String name;

    public Hero() {
        slots = new Slots();
        this.inventory = new ArrayList<>();
    }

    public Slots getSlots() {
        return slots;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getFinalMaxHP() {
        int headHpBonus = 0;
        int bodyHpBonus = 0;
        int legsHpBonus = 0;

        if (getSlots().getHead() != null) {
            headHpBonus = getSlots().getHead().getStats().getFinalHp(SlotType.HEAD);
        }

        if (getSlots().getBody() != null) {
            bodyHpBonus = getSlots().getBody().getStats().getFinalHp(SlotType.BODY);
        }

        if (getSlots().getLegs() != null) {
            legsHpBonus = getSlots().getLegs().getStats().getFinalHp(SlotType.LEGS);
        }

        return headHpBonus + bodyHpBonus + legsHpBonus + stats.getBaseHealth();
    }

    @Override
    public int getFinalStr() {
        int headStrBonus = 0;
        int bodyStrBonus = 0;
        int legsStrBonus = 0;

        if (getSlots().getHead() != null) {
            headStrBonus = getSlots().getHead().getStats().getFinalStr(SlotType.HEAD);
        }

        if (getSlots().getBody() != null) {
            bodyStrBonus = getSlots().getBody().getStats().getFinalStr(SlotType.BODY);
        }

        if (getSlots().getLegs() != null) {
            legsStrBonus = getSlots().getLegs().getStats().getFinalStr(SlotType.LEGS);
        }

        return headStrBonus + bodyStrBonus + legsStrBonus + stats.getBaseStrength();
    }

    @Override
    public int getFinalDex() {
        int headDexBonus = 0;
        int bodyDexBonus = 0;
        int legsDexBonus = 0;

        if (getSlots().getHead() != null) {
            headDexBonus = getSlots().getHead().getStats().getFinalDex(SlotType.HEAD);
        }

        if (getSlots().getBody() != null) {
            bodyDexBonus = getSlots().getBody().getStats().getFinalDex(SlotType.BODY);
        }

        if (getSlots().getLegs() != null) {
            legsDexBonus = getSlots().getLegs().getStats().getFinalDex(SlotType.LEGS);
        }

        return headDexBonus + bodyDexBonus + legsDexBonus + stats.getBaseDexterity();
    }

    @Override
    public int getFinalInt() {
        int headIntBonus = 0;
        int bodyIntBonus = 0;
        int legsIntBonus = 0;

        if (getSlots().getHead() != null) {
            headIntBonus = getSlots().getHead().getStats().getFinalInt(SlotType.HEAD);
        }

        if (getSlots().getBody() != null) {
            bodyIntBonus = getSlots().getBody().getStats().getFinalInt(SlotType.BODY);
        }

        if (getSlots().getLegs() != null) {
            legsIntBonus = getSlots().getLegs().getStats().getFinalInt(SlotType.LEGS);
        }

        return headIntBonus + bodyIntBonus + legsIntBonus + stats.getBaseIntelligence();
    }

    @Override
    public int getFinalDamageOutput() {
        int weaponDamage = 0;

        if (getSlots().getEquippedWeapon() == null)
            return weaponDamage;

        weaponDamage = getSlots().getEquippedWeapon().getDamage();

        return (int) switch (name) {
            case "Warrior" -> weaponDamage + getFinalStr() * 1.5;
            case "Ranger" -> weaponDamage + getFinalDex() * 2;
            case "Mage" -> weaponDamage + getFinalInt() * 3;
            default -> 0;
        };
    }
}