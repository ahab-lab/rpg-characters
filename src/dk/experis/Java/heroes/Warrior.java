package dk.experis.Java.heroes;

import dk.experis.Java.heroes.properties.Stats;

public class Warrior extends Hero {

    public Warrior() {
        setStats(new Stats(150, 10, 3, 1, 9, 0,
                30, 5, 2, 1));
        setName("Warrior");
    }
}