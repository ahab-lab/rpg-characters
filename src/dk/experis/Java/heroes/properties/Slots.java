package dk.experis.Java.heroes.properties;

import dk.experis.Java.equipments.armor.Armor;
import dk.experis.Java.equipments.armor.SlotType;
import dk.experis.Java.equipments.weapons.Weapon;

/**
 * this class handles the character slots and,
 *  updates the state of each item when equipped and unequipped
 */
public class Slots {
    private Weapon equippedWeapon;
    private Armor body;
    private Armor head;
    private Armor legs;

    public Slots() {
        equippedWeapon = null;
        //Armor slots
        body = null;
        head = null;
        legs = null;
    }

    public Weapon getEquippedWeapon() {
        return equippedWeapon;
    }

    public void setEquippedWeapon(Weapon equippedWeapon) {
        this.equippedWeapon = equippedWeapon;
    }

    public Armor getBody() {
        return body;
    }

    public void setBody(Armor body) {

        if (body == null) {

            if (this.body != null)
                this.body.getStats().setSlot(SlotType.INVENTORY);
        }
        else {
            body.getStats().setSlot(SlotType.BODY);
            System.out.println(body.getStats().getSlot());
        }
        this.body = body;
    }

    public Armor getHead() {
        return head;
    }

    public void setHead(Armor head) {

        if (head == null) {

            if (this.head != null)
                this.head.getStats().setSlot(SlotType.INVENTORY);
        }
        else {
            head.getStats().setSlot(SlotType.HEAD);
        }
        this.head = head;
    }

    public Armor getLegs() {
        return legs;
    }

    public void setLegs(Armor legs) {

        if (legs == null) {

            if (this.legs != null)
                this.legs.getStats().setSlot(SlotType.INVENTORY);
        }
        else {
            legs.getStats().setSlot(SlotType.LEGS);
        }
        this.legs = legs;
    }
}