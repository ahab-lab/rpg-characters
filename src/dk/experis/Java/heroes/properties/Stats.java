package dk.experis.Java.heroes.properties;

/**
 * To store and update stats accordingly
 * as the character gain experience
 */
public class Stats {
    private final int hpIncreasePercentage;
    private final int strIncreasePercentage;
    private final int dexIncreasePercentage;
    private final int intIncreasePercentage;
    private int baseHealth;
    private int baseStrength;
    private int baseDexterity;
    private int baseIntelligence;
    private int level;
    private int exp;
    private int nextLevelAtExp;

    public Stats(int health, int strength, int dexterity, int intelligence, int level, int exp,
                 int hpIncreasePercentage, int strIncreasePercentage, int dexIncreasePercentage, int intIncreasePercentage) {
        this.baseHealth = health;
        this.baseStrength = strength;
        this.baseDexterity = dexterity;
        this.baseIntelligence = intelligence;
        this.exp = updateNextLevelAtExp(level - 1);
        this.hpIncreasePercentage = hpIncreasePercentage;
        this.strIncreasePercentage = strIncreasePercentage;
        this.dexIncreasePercentage = dexIncreasePercentage;
        this.intIncreasePercentage = intIncreasePercentage;
        this.exp = exp;

        if (level != 1) {// update stats in case character is being instantiated with a level other than level 1
            for (int i = 0; i < level - 1; i++) {
                levelUP();
            }
            this.level++;
        }

        this.level = level;
        updateNextLevelAtExp(level);
        isNewLevelAvailable();
    }

    /**
     * trigger levelUP() if the amount of experience is sufficient to level up
     */
    public void isNewLevelAvailable() {

        while (exp >= nextLevelAtExp) {
            levelUP();
        }
    }

    /**
     * update stats upon leveling up
     */
    public void levelUP() {
        level++;
        baseHealth += hpIncreasePercentage;
        baseStrength += strIncreasePercentage;
        baseDexterity += dexIncreasePercentage;
        baseIntelligence += intIncreasePercentage;
        updateNextLevelAtExp(level);
    }

    /**
     * to get the needed amount of xp for the next level
     */
    public int getXpToNext() {
        return nextLevelAtExp - exp;
    }

    /**
     * calculates the need xp for the given level
     */
    public int updateNextLevelAtExp(int level) {
        int temp = 100;

        for (int i = 1; i < level; i++) {
            temp += (int) (temp * 0.1);
        }
        return this.nextLevelAtExp = temp;
    }

    public void addExp(int exp) {
        this.exp += exp;
    }

    public int getBaseHealth() {
        return baseHealth;
    }

    public int getBaseStrength() {
        return baseStrength;
    }

    public int getBaseDexterity() {
        return baseDexterity;
    }

    public int getBaseIntelligence() {
        return baseIntelligence;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}