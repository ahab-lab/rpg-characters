package dk.experis.Java.heroes.properties;

public interface TotalModifiedStats {
    int getFinalMaxHP();

    int getFinalStr();

    int getFinalDex();

    int getFinalInt();

    int getFinalDamageOutput();
}
