package dk.experis.Java.data;

import dk.experis.Java.equipments.armor.Armor;
import dk.experis.Java.equipments.armor.ArmorFactory;
import dk.experis.Java.equipments.weapons.Weapon;
import dk.experis.Java.equipments.weapons.WeaponFactory;

import java.util.ArrayList;

/**
 * this class create mock data to test the project functionality
 */
public class MockData {
    private final WeaponFactory weaponFactory;
    private final ArmorFactory armorFactory;

    public MockData() {
        weaponFactory = new WeaponFactory();
        armorFactory = new ArmorFactory();
    }

    public ArrayList<Weapon> getWeaponsList() {
        ArrayList<Weapon> weapons = new ArrayList<>();

        weapons.add(weaponFactory.makeWeapon("Desire's Mithril Cleaver", "melee"));
        weapons.add(weaponFactory.makeWeapon("Baneful Edge", "melee"));
        weapons.add(weaponFactory.makeWeapon("Grieving Willow Longbow", "range"));
        weapons.add(weaponFactory.makeWeapon("Yearning Bone Bow", "range"));
        weapons.add(weaponFactory.makeWeapon("Gladiator's Manuscript", "magic"));
        weapons.add(weaponFactory.makeWeapon("Peacekeeper's Epitome", "magic"));

        return weapons;
    }

    public ArrayList<Armor> getArmorList() {
        ArrayList<Armor> armorList = new ArrayList<>();

        armorList.add(armorFactory.makeArmor("The warden armor", "cloth"));
        armorList.add(armorFactory.makeArmor("Plate armor", "plate"));
        armorList.add(armorFactory.makeArmor("Cloth leggings of the Magi", "cloth"));
        armorList.add(armorFactory.makeArmor("Leather armor", "leather"));
        armorList.add(armorFactory.makeArmor("Plate Chest of the Juggernaut", "plate"));

        return armorList;
    }
}