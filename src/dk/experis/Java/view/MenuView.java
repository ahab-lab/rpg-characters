package dk.experis.Java.view;

import dk.experis.Java.data.MockData;
import dk.experis.Java.equipments.armor.Armor;
import dk.experis.Java.equipments.weapons.Weapon;
import dk.experis.Java.heroes.*;


public class MenuView {
    private final MockData mockData;
    private HeroFactory heroFactory;

    public MenuView() {
        mockData = new MockData();
        heroFactory = new HeroFactory();
    }

    public void mainMenu() {
        System.out.println("--------------------------");
        System.out.println("----- RPG Characters -----");
        System.out.println("--------------------------");
        System.out.println("1- Test preset characters");
        System.out.println("0- Exit");
        System.out.println("--------------------------");
    }

    public void presetCharactersMenu() {
        System.out.println("--------------------------");
        System.out.println("1- Warrior");
        System.out.println("2- ranger");
        System.out.println("3- Mage");
        System.out.println("0- Exit");
        System.out.println("--------------------------");
    }

    public void character1() {
        Hero  warrior = heroFactory.makeHero("warrior");

        printHeroInfo(warrior);

        warrior.getSlots().setHead(mockData.getArmorList().get(1));
        //warrior.getSlots().setBody(db.getArmorList().get(1));
        //warrior.getSlots().setLegs(db.getArmorList().get(1));
        warrior.getSlots().setEquippedWeapon(mockData.getWeaponsList().get(0));
        warrior.getStats().isNewLevelAvailable();

        printArmorInfo(warrior.getSlots().getHead());
        printArmorInfo(warrior.getSlots().getBody());
        printArmorInfo(warrior.getSlots().getLegs());
        printWeaponInfo(mockData.getWeaponsList().get(0));
        printHeroInfo(warrior);
    }

    public void character2() {
        Hero ranger = heroFactory.makeHero("ranger");

        printHeroInfo(ranger);

        ranger.getSlots().setHead(mockData.getArmorList().get(1));
        //warrior.getSlots().setBody(db.getArmorList().get(1));
        //warrior.getSlots().setLegs(db.getArmorList().get(1));
        ranger.getSlots().setEquippedWeapon(mockData.getWeaponsList().get(0));
        ranger.getStats().isNewLevelAvailable();

        printArmorInfo(ranger.getSlots().getHead());
        printArmorInfo(ranger.getSlots().getBody());
        printArmorInfo(ranger.getSlots().getLegs());
        printWeaponInfo(mockData.getWeaponsList().get(0));
        printHeroInfo(ranger);
    }

    public void character3() {
        Hero mage = heroFactory.makeHero("mage");

        printHeroInfo(mage);

        mage.getSlots().setHead(mockData.getArmorList().get(3));
        //warrior.getSlots().setBody(db.getArmorList().get(1));
        //warrior.getSlots().setLegs(db.getArmorList().get(1));
        mage.getSlots().setEquippedWeapon(mockData.getWeaponsList().get(3));
        mage.getStats().isNewLevelAvailable();

        printArmorInfo(mage.getSlots().getHead());
        printArmorInfo(mage.getSlots().getBody());
        printArmorInfo(mage.getSlots().getLegs());
        printWeaponInfo(mockData.getWeaponsList().get(0));
        printHeroInfo(mage);
    }


    public void printHeroInfo(Hero hero) {
        System.out.println("****************************");
        System.out.println(hero.getName() + " details: ");
        System.out.println("HP: " + hero.getFinalMaxHP());
        System.out.println("Str: " + hero.getFinalStr());
        System.out.println("Dex: " + hero.getFinalDex());
        System.out.println("Int: " + hero.getFinalInt());
        System.out.println("Lvl: " + hero.getStats().getLevel());
        System.out.println("XP to next: " + hero.getStats().getXpToNext());
        System.out.println("Head: " + ((hero.getSlots().getHead() != null) ? hero.getSlots().getHead().getName() : "None"));
        System.out.println("Body: " + ((hero.getSlots().getBody() != null) ? hero.getSlots().getBody().getName() : "None"));
        System.out.println("Legs: " + ((hero.getSlots().getLegs() != null) ? hero.getSlots().getLegs().getName() : "None"));
        System.out.println("****************************");

        attack(hero.getFinalDamageOutput());
    }

    public void printArmorInfo(Armor armor) {

        if (armor == null)
            return;

        System.out.println("Stats for: " + armor.getName());
        System.out.println("Armor Type: " + armor.getType());
        System.out.println("Slot: " + armor.getStats().getSlot());
        System.out.println("Armor level: " + armor.getLevel());

        if (armor.getStats().getHpBonus() != 0)
            System.out.println("Bonus HP: " + armor.getStats().getHpBonus());

        if (armor.getStats().getStrBonus() != 0)
            System.out.println("Bonus Str: " + armor.getStats().getStrBonus());

        if (armor.getStats().getDexBonus() != 0)
            System.out.println("Bonus Dex: " + armor.getStats().getDexBonus());

        if (armor.getStats().getIntBonus() != 0)
            System.out.println("Bonus Int: " + armor.getStats().getIntBonus());
    }

    public void printWeaponInfo(Weapon weapon) {
        if (weapon == null)
            return;
        System.out.println("****************************");
        System.out.println("Item Stats for: " + weapon.getName());
        System.out.println("Weapon Type: " + weapon.getType());
        System.out.println("Weapon level: " + weapon.getLevel());
        System.out.println("Damage: " + weapon.getDamage());
    }

    public void attack(int damage) {

        if (damage == 0)
            return;

        System.out.printf("Attacking for %d%n", +damage);
    }
}