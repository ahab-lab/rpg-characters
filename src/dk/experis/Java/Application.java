package dk.experis.Java;

import dk.experis.Java.view.MenuView;

import java.util.Scanner;

public class Application {

    private final Scanner scanner;
    private final MenuView menu;

    public Application() {
        scanner = new Scanner(System.in);
        menu = new MenuView();
    }

    public void run() {
        String ans;

        do {
            menu.mainMenu();
            ans = scanner.nextLine();

            switch (ans) {
                case "1" -> {
                    do {
                        menu.presetCharactersMenu();
                        ans = scanner.nextLine();

                        try {
                            Integer.parseInt(ans);
                            break;
                        }
                        catch (NumberFormatException e) {
                            System.out.println("Please use integers.");
                        }
                    } while (true);

                    switch (ans) {
                        case "1" -> menu.character1();
                        case "2" -> menu.character2();
                        case "3" -> menu.character3();

                        default -> System.out.println("Please choose one of the available options.");
                    }
                }
                case "0" -> System.out.println("Bye!");
                default -> System.out.println("Select one of the  available options");
            }

        } while (!ans.equals("0"));
    }
}